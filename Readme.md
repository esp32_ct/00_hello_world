Hello world
===

*Framework*: Espressif idf  
*IDE*: Visual Studio Code - Platformio  


Proyecto incial que utiliza el led integrado en la tarjeta (Pin 2) para mostrar  
el parpadeo del led en periodos de 500ms.  
Ademas muestra en el monitor serial el mensaje hello world.
